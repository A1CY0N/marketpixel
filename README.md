# MarketPixel

> Flutter application created for entrepreneurship courses at Tallinn University of Technology.

![](assets/logo.png)

MarketPixel is a platform that allows you to scan products in a supermarket to read their composition more easily. It is mainly intended for people who doesn't speak the languages present on the product labels.

You will find on my github the [landing page of marketPixel](https://github.com/A1CY0N/marketPixel_website) as well as its [documentation](https://github.com/A1CY0N/marketPixel_doc).

## How to compile the app ?
IONIC project (angular / typescript / cordova)

To compile this application you just need to install [Flutter](https://flutter.dev/docs/get-started/install).
As well as the software stack corresponding to your destination platform (android or iOS)

Once it's done, download sources :
```sh
git clone https://gitlab.com/A1CY0N/marketpixel.git
```
And run following commands to generate the bundle :
```sh
flutter clean && flutter build appbundle --release
``` 
The release bundle for your app is created at ```/build/app/outputs/bundle/release/app.aab```.

By default, the app bundle contains Dart code and the Flutter runtime compiled for armeabi-v7a (ARM 32-bit), arm64-v8a (ARM 64-bit), and x86-64 (x86 64-bit)

If you want to build APK, run :
```sh
flutter build apk --split-per-abi
```

## Application overview
![App overview](assets/screenshots/1.png "App overview")
## Meta

* [@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com
* Ivar V.
* Christophe de G.

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/marketpixel>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request